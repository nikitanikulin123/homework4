'use strict';

const fs = require('fs');
var crypto = require('crypto');

const input = fs.createReadStream("input.txt");
const output = fs.createWriteStream("output.txt");
const Transform   = require("stream").Transform;

var md5 = crypto.createHash('md5'), md5Output;

class HTransform extends Transform {
    constructor(options) {
        super(options);

        this.hash = md5;
    }

    _transform(chunk, encoding, done){
        //this.push(chunk);
        let result = this.hash.update(chunk).digest('hex');
        console.log(result);
        done(null, result);
    }
}

const tr = new HTransform();

// ����� 1
//input.pipe(md5).pipe(process.stdout);
//input.pipe(md5).pipe(output);

// ����� 2
input.pipe(tr).pipe(output);